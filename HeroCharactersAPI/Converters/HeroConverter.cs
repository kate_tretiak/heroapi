﻿using System;
using System.Collections.Generic;
using System.Linq;
using HeroCharactersAPI.Models;
using Newtonsoft.Json.Linq;

namespace HeroCharactersAPI.Converters
{
    public class HeroConverter
    {
        public static IEnumerable<Hero> Convert(JObject responseData)
        {
            IList<JToken> results = responseData["data"]["results"].Children().ToList();

            List<Hero> heroes = new List<Hero>();

            foreach (JToken token in results)
            {
                var hero = token.ToObject<Hero>();

                heroes.Add(hero);
            }

            return heroes;
        }
    }
}