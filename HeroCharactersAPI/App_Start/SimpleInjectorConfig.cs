﻿using System;
using System.Net.Http;
using System.Web.Http;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;
using HeroCharactersAPI.Services;

namespace HeroCharactersAPI
{
    public class SimpleInjectorConfig
    {
        public static void Configure()
        {
            var container = new SimpleInjector.Container();
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            container.Register<IHeroService, HeroService>(Lifestyle.Singleton);
            container.RegisterSingleton(() => new HttpClient()
            {
                BaseAddress = new Uri(container.GetInstance<Models.Configuration>().BaseUrl)
            });
            container.Register<Models.Configuration>(Lifestyle.Singleton);
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
            container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}