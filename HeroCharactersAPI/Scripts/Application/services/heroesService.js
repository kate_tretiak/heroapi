﻿angular.module('HeroesApp').service('heroesService',
    [
        '$http', function($http) {
            this.getHeroes = function (id = null, pageIndex = 0, pageSize = 20) {
                if (id !== null) {
                    return $http.get('api/HeroesData/'+id);
                }

                return $http.get('api/HeroesData?pageIndex='+pageIndex+'&pageSize='+ pageSize);
            }
        }
    ]);