﻿angular.module('HeroesApp').controller('heroDetailsController',
    [
        '$scope', 'heroesService', function ($scope, heroesService, $uibModalInstance) {
            $scope.hero = [];
            $scope.isDataLoaded = false;

            getHero = function(id) {
                return heroesService.getHeroes(id).then(function(data) {
                    $scope.isDataLoaded = true;
                    $scope.hero = data.data;
                });
            };

            $scope.close = function() {
                $scope.modalInstance.close();
            }

            getHero($scope.$resolve.id);
        }
    ]);