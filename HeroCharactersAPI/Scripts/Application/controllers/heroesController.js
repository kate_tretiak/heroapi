﻿angular.module('HeroesApp').controller('heroesController',
    [
        '$scope', 'heroesService', '$uibModal', function ($scope, heroesService, $uibModal) {
            $scope.heroes = [];
            $scope.isDataLoaded = false;
            $scope.currentPage = 1;
            $scope.modalInstance = null;

            function getHeroes() {
                heroesService.getHeroes().then(function(data) {
                    $scope.isDataLoaded = true;
                    $scope.heroes = data.data;
                });
            }

            $scope.getHero = function(id) {
                $scope.modalInstance = $uibModal.open({
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    title: "Marvel Hero Details",
                    templateUrl: 'Scripts/Application/templates/HeroDetails.html',
                    controller: 'heroDetailsController',
                    controllerAs: 'heroDetails',
                    scope: $scope,
                    resolve: {
                        id: function () {
                            return id;
                        }
                    }
                });

                $scope.modalInstance.result.then(function () {
                    modalInstance.close();
                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            }

            $scope.pageChanged = function (currentPage) {
                heroesService.getHeroes(null, currentPage, 20).then(function (data) {
                    $scope.heroes = data.data;
                    $scope.currentPage = currentPage;
                });
            }
            getHeroes();
        }
    ]);