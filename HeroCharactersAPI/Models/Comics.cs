﻿using System.Collections.Generic;

namespace HeroCharactersAPI.Models
{
    public class Comics
    {
        public int Available { get; set; }
        public string CollectionURI { get; set; }
        public List<ComicsSummary> Items { get; set; }
        public int Returned { get; set; }
    }
}