﻿using System.Collections.Generic;

namespace HeroCharactersAPI.Models
{
    public class Stories
    {
        public int Available { get; set; }
        public string CollectionURI { get; set; }
        public List<StoriesSummary> Items { get; set; }
        public int Returned { get; set; }
    }
}