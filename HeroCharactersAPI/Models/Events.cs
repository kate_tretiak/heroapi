﻿using System.Collections.Generic;

namespace HeroCharactersAPI.Models
{
    public class Events
    {
        public int Available { get; set; }
        public string CollectionURI { get; set; }
        public List<EventSummary> Items { get; set; }
        public int Returned { get; set; }
    }
}