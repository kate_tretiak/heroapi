﻿namespace HeroCharactersAPI.Models
{
    public class URL
    {
        public string Type { get; set; }
        public string Url { get; set; }
    }
}