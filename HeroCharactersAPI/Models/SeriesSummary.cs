﻿namespace HeroCharactersAPI.Models
{
    public class SeriesSummary
    {
        public string ResourceURI { get; set; }
        public string Name { get; set; }
    }
}