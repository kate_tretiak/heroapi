﻿using System.Collections.Generic;

namespace HeroCharactersAPI.Models.Representation
{
    public class HeroDetails
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ThumbnailSource { get; set; }

        public string Extension { get; set; }

        public List<ComicsSummary> Items { get; set; }
    }
}