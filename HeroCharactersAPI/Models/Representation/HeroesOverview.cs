﻿using System.Collections.Generic;

namespace HeroCharactersAPI.Models.Representation
{
    public class HeroesOverview
    {
        public int Total { get; set; }
        public IEnumerable<HeroOverview> Heroes { get; set; }
    }
}