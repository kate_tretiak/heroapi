﻿namespace HeroCharactersAPI.Models
{
    public class EventSummary
    {
        public string ResourceURI { get; set; }
        public string Name { get; set; }
    }
}