﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HeroCharactersAPI.Models
{
    public class HeroesOverviewDomain
    {
        public int Total { get; set; }

        public IEnumerable<Hero> Heroes { get; set; }
    }
}