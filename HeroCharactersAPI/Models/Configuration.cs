﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using HeroCharactersAPI.Extensions;

namespace HeroCharactersAPI.Models
{
    public class Configuration
    {
        public string BaseUrl { get; } = ConfigurationManager.AppSettings["MarvelAPIBaseUrl"];

        public string Hash => (TimeStamp + PrivateKey + PublicKey).ToMD5Hash();

        public string PublicKey { get; } = ConfigurationManager.AppSettings["PublicAPIKey"];

        public string PrivateKey { get; } = ConfigurationManager.AppSettings["PrivateAPIKey"];

        public string TimeStamp { get; } = ConfigurationManager.AppSettings["TimeStamp"];
    }
}