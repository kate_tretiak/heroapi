﻿namespace HeroCharactersAPI.Models
{
    public class ComicsSummary
    {
        public string ResourceURI { get; set; }
        public string Name { get; set; }
    }
}