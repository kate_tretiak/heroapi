﻿namespace HeroCharactersAPI.Models
{
    public class StoriesSummary
    {
        public string ResourceURI { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}