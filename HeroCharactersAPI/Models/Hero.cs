﻿using System.Collections.Generic;

namespace HeroCharactersAPI.Models
{
    public class Hero
    {

        public Hero(int id, string name, string description, Thumbnail thumbnail,
        string resourceURI, Comics comics, Series series, Stories stories, Events events,
            List<URL> urls)
        {
            this.Id = id;
            this.Name = name;
            this.Description = description;
            this.Thumbnail = thumbnail;
            this.ResourceURI = resourceURI;
            this.Comics = comics;
            this.Series = series;
            this.Stories = stories;
            this.Events = events;
            this.Urls = urls;
        }

        public int Id { get; }
        public string Name { get; }
        public string Description { get; }
        public Thumbnail Thumbnail { get; }
        public string ResourceURI { get; }
        public Comics Comics { get; }
        public Series Series { get; }
        public Stories Stories { get; }
        public Events Events { get; }
        public List<URL> Urls { get; }
    }
}