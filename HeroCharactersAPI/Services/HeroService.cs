﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using HeroCharactersAPI.Converters;
using HeroCharactersAPI.Models;
using Newtonsoft.Json.Linq;
using Configuration = HeroCharactersAPI.Models.Configuration;

namespace HeroCharactersAPI.Services
{
    public sealed class HeroService : IHeroService
    {
        private readonly HttpClient _client;
        private Configuration _configuration;

        public HeroService(HttpClient client, Configuration configuration)
        {
            _client = client;
            _configuration = configuration;
        }

        public async Task<HeroesOverviewDomain> GetHeroes(int offset)
        {
            var url = $"{_configuration.BaseUrl + ConfigurationManager.AppSettings["HeroesAPIUrl"]}" +
                      $"?offset={offset}&ts={_configuration.TimeStamp}&apikey={_configuration.PublicKey}" +
                      $"&hash={_configuration.Hash}";

            var jsonResponse = await _client.GetStringAsync(url);
            var jObject = JObject.Parse(jsonResponse);
            IEnumerable<Hero> heroesData = HeroConverter.Convert(jObject);
            return new HeroesOverviewDomain()
            {
                Total = Convert.ToInt32(jObject["data"]["total"]),
                Heroes = heroesData
            };
        }

        public async Task<Hero> GetHero(int id)
        {
            var url = $"{_configuration.BaseUrl + ConfigurationManager.AppSettings["HeroesAPIUrl"]}" +
                      $"/{id}"+
                      $"?ts={_configuration.TimeStamp}&apikey={_configuration.PublicKey}" +
                      $"&hash={_configuration.Hash}";

            var jsonResponse = await _client.GetStringAsync(url);
            var jObject = JObject.Parse(jsonResponse);
            Hero heroesData = HeroConverter.Convert(jObject).FirstOrDefault();
            return heroesData;
        }
    }
}