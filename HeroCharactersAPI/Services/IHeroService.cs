﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HeroCharactersAPI.Models;

namespace HeroCharactersAPI.Services
{
    public interface IHeroService
    {
        Task<HeroesOverviewDomain> GetHeroes(int offset);

        Task<Hero> GetHero(int id);
    }
}
