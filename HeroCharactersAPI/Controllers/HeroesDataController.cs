﻿using System;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using System.Web.Http;
using HeroCharactersAPI.Models.Representation;
using HeroCharactersAPI.Services;

namespace HeroCharactersAPI.Controllers
{
    public class HeroesDataController : ApiController
    {
        private readonly IHeroService _heroService;
        public HeroesDataController(IHeroService heroService)
        {
            this._heroService = heroService;
        }

        public async Task<HeroesOverview> Get(int pageIndex, int pageSize)
        {
            var cacheKey = $"HeroesData-{pageIndex}:{pageSize}";
            var heroesData = MemoryCache.Default.Get(cacheKey) as Models.HeroesOverviewDomain;
            if (heroesData == null)
            {
                int offset = pageIndex > 1 ? pageSize * pageIndex : 0;
                heroesData = await _heroService.GetHeroes(offset);
                MemoryCache.Default.Add(cacheKey, heroesData, new DateTimeOffset(DateTime.Now.AddHours(1)));
            }

            return new Models.Representation.HeroesOverview()
            {
                Total = heroesData.Total,
                Heroes = heroesData.Heroes.Select(h => new HeroOverview()
                {
                    Id = h.Id,
                    Name = h.Name,
                    ThumbnailSource = h.Thumbnail.Path,
                    Extension = h.Thumbnail.Extension,
                    Urls = h.Urls
                })
            };
        }

        public async Task<HeroDetails> Get(int id)
        {
            var hero = await this._heroService.GetHero(id);
            return new HeroDetails()
            {
                Id = hero.Id,
                Name = hero.Name,
                Description = hero.Description,
                ThumbnailSource = hero.Thumbnail.Path,
                Extension = hero.Thumbnail.Extension,
                Items = hero.Comics.Items
            };
        }
    }
}
